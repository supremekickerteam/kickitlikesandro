import { Injectable } from '@angular/core';

import { HubProxy } from './signalr/hub-proxy';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TableSoccerService extends HubProxy {

  private modelChangedSubject = new Subject();

  modelChanged$ = this.modelChangedSubject.asObservable();

  constructor() {
    super('tableSoccerHub');
    this.on('modelChanged', () => this.modelChangedSubject.next());
  }
}
