import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MainComponent} from './main/main.component';
import {ScoreboardComponent} from './scoreboard/scoreboard.component';

const spontaneousMatchRoutes: Routes = [
  {
    path: 'spontaneousMatches',
    component: MainComponent,
    children: [
      { path: '', component: ScoreboardComponent },
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(spontaneousMatchRoutes),
  ],
  exports: [
    RouterModule,
  ]
})
export class SpontaneousMatchRoutingModule { }
