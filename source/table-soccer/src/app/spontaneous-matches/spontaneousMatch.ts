export class SpontaneousMatch {
  timeStampCreated: string;
  scoreRed: number = 0;
  scoreBlue: number = 0;
}
