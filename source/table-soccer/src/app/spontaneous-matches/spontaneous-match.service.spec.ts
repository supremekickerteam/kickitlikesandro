import { TestBed, inject } from '@angular/core/testing';

import { SpontaneousMatchService } from './spontaneous-match.service';

describe('SpontaneousMatchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpontaneousMatchService]
    });
  });

  it('should be created', inject([SpontaneousMatchService], (service: SpontaneousMatchService) => {
    expect(service).toBeTruthy();
  }));
});
