import { Injectable } from '@angular/core';
import {SpontaneousMatch} from './spontaneousMatch';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {getHostApiUrl} from '../util';

function getUrl() {
  return getHostApiUrl() + 'spontaneousMatch';  // TODO use environment variable instead!
}

@Injectable({
  providedIn: 'root'
})
export class SpontaneousMatchService {
  constructor(private http: HttpClient) {}

  getCurrentSpontaneousMatch(): Observable<SpontaneousMatch>{
    return this.http.get<SpontaneousMatch>(getUrl());
  }
}
