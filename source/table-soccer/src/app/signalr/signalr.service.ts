import { Inject, Injectable, NgZone } from '@angular/core';

import { HubProxy } from './hub-proxy';
import {getHostApiUrl} from '../util';

import 'signalr';

function getSignalRUrl(){
  return getHostApiUrl() + 'signalr'; // TODO use environment variable instead!
}

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  private readonly connection_: any;
  private shouldBeConnected_ = false;

  constructor(@Inject(HubProxy) private proxies_: HubProxy[], private zone_: NgZone) {

    console.log('SignalR: SignalRService() with the following proxies:');
    this.proxies_.forEach(p => console.log(`SignalR: - ${p.name}`));

    this.connection_ = $.hubConnection(getSignalRUrl(), { useDefaultPath: false });

    this.proxies_.forEach(p => this.setupProxy(p));

    this.connection_.connectionSlow(() => console.log('SignalR: connection slow'));
    this.connection_.reconnecting(() => console.log('SignalR: reconnecting'));
    this.connection_.reconnected(this.handleReconnected.bind(this));
    this.connection_.disconnected(this.handleDisconnected.bind(this));
  }

  get connected() {
    return this.connection_ !== null && this.connection_.state === $.signalR.connectionState.connected;
  }

  setBearerToken(token: string) {
    this.connection_.qs = {'bearer_token': token};
  }

  start(): Promise<boolean> {
    console.log('SignalR: start()');

    this.shouldBeConnected_ = true;

    return this.connection_.start()
      .promise()
      .then(() => this.handleConnected())
      .catch(err => {
        console.log(`SignalR: error while starting: ${err}`);
        return Promise.resolve(false);
      });
  }

  stop() {
    console.log('SignalR: stop()');
    this.shouldBeConnected_ = false;
    this.connection_.stop();
  }

  private setupProxy(proxy: HubProxy) {
    const rawProxy = this.connection_.createHubProxy(proxy.name);
    proxy.setHandlerRegistry(rawProxy, this.zone_);
  }

  private handleConnected(): boolean {
    console.log('SignalR: connected, transport: ' + this.connection_.transport.name);
    return this.connected;
  }

  private handleDisconnected() {
    console.log('SignalR: disconnected');

    if (this.shouldBeConnected_) {
      console.log('SignalR: trying to re-start the connection');

      setTimeout(() => this.start(), 5000);
    }
  }

  private handleReconnected() {
    console.log('SignalR: reconnected, transport: ' + this.connection_.transport.name);
    this.proxies_.forEach(p => p.onReconnected());
  }

}
