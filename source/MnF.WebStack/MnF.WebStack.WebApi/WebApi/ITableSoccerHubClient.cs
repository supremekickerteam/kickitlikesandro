﻿//-----------------------------------------------------------------------
// <copyright file="ITableSoccerHubClient.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.WebApi.WebApi
{
    /// <summary>
    /// Interface to clients of the <see cref="TableSoccerHub"/>.
    /// </summary>
    public interface ITableSoccerHubClient
    {
        /// <summary>
        /// Notifies changes on the data model.
        /// </summary>
        void ModelChanged();
    }
}
