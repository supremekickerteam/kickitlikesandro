﻿//-----------------------------------------------------------------------
// <copyright file="EventPusher.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.WebApi.WebApi
{
    using System.Threading.Tasks;

    using Microsoft.AspNet.SignalR;

    using MnF.WebStack.TableSoccer.Business;

    /// <summary>
    /// Pushes data to clients
    /// </summary>
    public class EventPusher
    {
        private readonly INotifier notifier;    // this is needed to instantiate the object and keep it alive

        private readonly IHubContext<ITableSoccerHubClient> context;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventPusher" /> class.
        /// </summary>
        /// <param name="notifier">The notifier.</param>
        /// <param name="context">The context.</param>
        public EventPusher(INotifier notifier, IHubContext<ITableSoccerHubClient> context)
        {
            this.context = context;
            this.notifier = notifier;
            notifier.ModelChanged += this.OnModelChanged;
        }

        public Task OnModelChanged()
        {
            this.context.Clients.All.ModelChanged();
            return Task.CompletedTask;
        }
    }
}
