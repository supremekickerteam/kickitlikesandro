﻿//-----------------------------------------------------------------------
// <copyright file="AutofacModule.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.WebApi
{
    using Autofac;

    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Infrastructure;

    using MnF.WebStack.WebApi.WebApi;

    using Module = Autofac.Module;

    /// <summary>
    /// The <see cref="Autofac"/> module if the <see cref="MnF.WebStack.TableSoccer"/> component.
    /// </summary>
    public sealed class AutofacModule : Module
    {
        /// <summary>
        /// Override to add registrations to the container.
        /// </summary>
        /// <param name="builder">The builder through which components can be
        /// registered.</param>
        /// <remarks>
        /// Note that the ContainerBuilder parameter is unique to this module.
        /// </remarks>
        protected override void Load(ContainerBuilder builder)
        {
            RegisterWebApi(builder);
        }

        private static void RegisterWebApi(ContainerBuilder builder)
        {
            builder.RegisterType<PlayerController>()
                .InstancePerRequest();

            builder.RegisterType<MatchController>()
                .InstancePerRequest();

            builder.RegisterType<SetController>()
                .InstancePerRequest();

            builder.RegisterType<SpontaneousMatchController>()
                .InstancePerRequest();

            builder.RegisterType<TableSoccerHub>()
                .ExternallyOwned();

            builder.Register(c => c.Resolve<IDependencyResolver>()
                    .Resolve<IConnectionManager>()
                    .GetHubContext<TableSoccerHub, ITableSoccerHubClient>())
                .ExternallyOwned();

            builder.RegisterType<EventPusher>().SingleInstance();
        }
    }
}
