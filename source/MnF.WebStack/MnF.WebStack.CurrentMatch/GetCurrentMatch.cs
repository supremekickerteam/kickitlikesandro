
namespace MnF.WebStack.CurrentMatch
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Azure.WebJobs.Host;

    using MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc;
    using MnF.WebStack.TableSoccer.Business;

    /// <summary>
    /// Rest API to get the current match
    /// </summary>
    public static class GetCurrentMatch
    {
        /// <summary>
        /// Runs the specified req.
        /// </summary>
        /// <param name="req">The req.</param>
        /// <param name="log">The log.</param>
        /// <param name="matchService">The match service.</param>
        /// <returns>The current match</returns>
        [FunctionName("GetCurrentMatch")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)]HttpRequestMessage req,
            TraceWriter log,
            [Inject]  ISpontaneousMatchService matchService
            )
        {

            var currentMatch = await matchService.GetCurrentMatch();
            return req.CreateResponse(HttpStatusCode.OK, currentMatch);
        }
    }
}
