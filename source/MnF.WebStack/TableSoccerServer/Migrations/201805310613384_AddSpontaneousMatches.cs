//-----------------------------------------------------------------------
// <copyright file="201805310613384_AddSpontaneousMatches.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Creates a table for spontaneous matches
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class AddSpontaneousMatches : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.CreateTable(
                "dbo.SpontaneousMatches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeStampCreated = c.DateTime(nullable: false),
                        TimeStampCompleted = c.DateTime(),
                        ScoreBlue = c.Int(nullable: false),
                        ScoreRed = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropTable("dbo.SpontaneousMatches");
        }
    }
}
