//-----------------------------------------------------------------------
// <copyright file="201805240854553_AddDeletedFlagsToMatchAndSet.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Add a delete flag on the match and set tables
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class AddDeletedFlagsToMatchAndSet : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.AddColumn("dbo.MatchChanges", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.Matches", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.Sets", "IsDeleted", c => c.Boolean(nullable: false));
            this.AddColumn("dbo.SetChanges", "IsDeleted", c => c.Boolean(nullable: false));
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropColumn("dbo.SetChanges", "IsDeleted");
            this.DropColumn("dbo.Sets", "IsDeleted");
            this.DropColumn("dbo.Matches", "IsDeleted");
            this.DropColumn("dbo.MatchChanges", "IsDeleted");
        }
    }
}
