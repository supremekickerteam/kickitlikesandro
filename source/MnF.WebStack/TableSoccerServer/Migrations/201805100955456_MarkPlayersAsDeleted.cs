//-----------------------------------------------------------------------
// <copyright file="201805100955456_MarkPlayersAsDeleted.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Add a column to mark players as deleted.
    /// </summary>
    public partial class MarkPlayersAsDeleted : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.AddColumn("dbo.Players", "IsDeleted", c => c.Boolean(nullable: false));
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropColumn("dbo.Players", "IsDeleted");
        }
    }
}
