//-----------------------------------------------------------------------
// <copyright file="201803201528321_InitialCreate.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Initial creation of the database.
    /// </summary>
    public partial class InitialCreate : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.CreateTable(
                "dbo.Matches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeStamp = c.DateTime(nullable: false),
                        RedPlayerOneId = c.Int(nullable: false),
                        RedPlayerTwoId = c.Int(nullable: false),
                        BluePlayerOneId = c.Int(nullable: false),
                        BluePlayerTwoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.BluePlayerOneId)
                .ForeignKey("dbo.Players", t => t.BluePlayerTwoId)
                .ForeignKey("dbo.Players", t => t.RedPlayerOneId)
                .ForeignKey("dbo.Players", t => t.RedPlayerTwoId)
                .Index(t => t.RedPlayerOneId)
                .Index(t => t.RedPlayerTwoId)
                .Index(t => t.BluePlayerOneId)
                .Index(t => t.BluePlayerTwoId);

            this.CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.Sets",
                c => new
                    {
                        MatchId = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        RedScore = c.Int(nullable: false),
                        BlueScore = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MatchId, t.Number })
                .ForeignKey("dbo.Matches", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId);
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.Sets", "MatchId", "dbo.Matches");
            this.DropForeignKey("dbo.Matches", "RedPlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.Matches", "RedPlayerOneId", "dbo.Players");
            this.DropForeignKey("dbo.Matches", "BluePlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.Matches", "BluePlayerOneId", "dbo.Players");
            this.DropIndex("dbo.Sets", new[] { "MatchId" });
            this.DropIndex("dbo.Matches", new[] { "BluePlayerTwoId" });
            this.DropIndex("dbo.Matches", new[] { "BluePlayerOneId" });
            this.DropIndex("dbo.Matches", new[] { "RedPlayerTwoId" });
            this.DropIndex("dbo.Matches", new[] { "RedPlayerOneId" });
            this.DropTable("dbo.Sets");
            this.DropTable("dbo.Players");
            this.DropTable("dbo.Matches");
        }
    }
}
