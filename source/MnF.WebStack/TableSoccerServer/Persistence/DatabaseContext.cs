﻿//-----------------------------------------------------------------------
// <copyright file="DatabaseContext.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace TableSoccerServer.Persistence
{
    using System.Data.Entity;

    using MnF.WebStack.TableSoccer.DomainModel;

    /// <summary>
    /// The table soccer database context.
    /// </summary>
    internal sealed class DatabaseContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseContext"/> class.
        /// </summary>
        public DatabaseContext()
            : base("TableSoccerDatabase")
        {
        }

        /// <summary>
        /// Gets or sets the matches.
        /// </summary>
        public IDbSet<Match> Matches { get; set; }

        /// <summary>
        /// Gets or sets the sets.
        /// </summary>
        public IDbSet<Set> Sets { get; set; }

        /// <summary>
        /// Gets or sets the players.
        /// </summary>
        public IDbSet<Player> Players { get; set; }

        /// <summary>
        /// Gets or sets the match changes.
        /// </summary>
        public IDbSet<MatchChange> MatchChanges { get; set; }

        /// <summary>
        /// Gets or sets the set changes.
        /// </summary>
        public IDbSet<SetChange> SetChanges { get; set; }

        /// <summary>
        /// Gets or sets the spontaneous matches.
        /// </summary>
        public IDbSet<SpontaneousMatch> SpontaneousMatches { get; set; }

        /// <summary>
        /// Gets or sets the history of spontaneous matches.
        /// </summary>
        public IDbSet<SpontaneousMatchHistory> SpontaneousMatchHistory { get; set; }

        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureTableSoccer();
        }
    }
}
