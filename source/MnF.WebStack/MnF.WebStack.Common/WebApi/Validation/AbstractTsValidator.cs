﻿//-----------------------------------------------------------------------
// <copyright file="AbstractTsValidator.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.WebApi.Validation
{
    using System.Data.Entity;
    using System.Net;
    using System.Web.Http.Controllers;
    using FluentValidation;

    using MnF.WebStack.Common.DataAccess;

    /// <summary>
    /// default implementation for <see cref="ITsValidator" />
    /// </summary>
    /// <typeparam name="TResource">The type to be validated</typeparam>
    /// <typeparam name="TDomain">The type of the domain.</typeparam>
    /// <seealso cref="FluentValidation.AbstractValidator{TResource}" />
    /// <seealso cref="FluentValidation.AbstractValidator{T}" />
    /// <seealso cref="MnF.WebStack.Common.WebApi.Validation.ITsValidator" />
    public abstract class AbstractTsValidator<TResource, TDomain> : AbstractValidator<TResource>, ITsValidatorGeneric<TResource>
        where TDomain : class
        where TResource : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractTsValidator{TResource, TDomain}" /> class.
        /// the weird construct with s.GetHashCode() is to make sure that input and output of the lambda do not have the same type because this breaks ShouldHaveValidationErrorFor needed for testing
        /// </summary>
        /// <param name="repository">The entities.</param>
        protected AbstractTsValidator(IRepository<TDomain> repository)
        {
            var entities = repository.Entities;

            this.RuleFor(s => s.GetHashCode()).Must((s, _) => this.DoesEntryExist(s, entities)) // "s.GetHashCode()": see summary
                .When(s => this.IsPut)
                .WithMessage(s => $"There is no entry in the database matching this primary key");
        }

        /// <summary>
        /// Gets a value indicating whether this instance is post.
        /// </summary>
        protected bool IsPost => this.HttpMethod == WebRequestMethods.Http.Post;

        /// <summary>
        /// Gets the HTTP method.
        /// </summary>
        private string HttpMethod => this.Context?.Request.Method.Method;

        /// <summary>
        /// Gets a value indicating whether this instance is put.
        /// </summary>
        private bool IsPut => this.HttpMethod == WebRequestMethods.Http.Put;

        private HttpActionContext Context { get; set; }

        /// <summary>
        /// Sets the HTTP action context.
        /// </summary>
        /// <param name="context">The context.</param>
        public void SetHttpActionContext(HttpActionContext context)
        {
            this.Context = context;     // this should be ok because every request gets a new instance of the validator => no concurrency problems
        }

        /// <summary>
        /// See "returns" part
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <param name="entities">The entities.</param>
        /// <returns>true if the entry exists in the database</returns>
        protected abstract bool DoesEntryExist(TResource resource, IDbSet<TDomain> entities);
    }
}
