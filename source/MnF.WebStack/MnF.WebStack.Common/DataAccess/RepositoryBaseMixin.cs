﻿//-----------------------------------------------------------------------
// <copyright file="RepositoryBaseMixin.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Mixin class for repositories.
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public class RepositoryBaseMixin<TContext> : IDisposable
        where TContext : DbContext, new()
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBaseMixin{TContext}"/> class.
        /// </summary>
        protected RepositoryBaseMixin()
        {
            this.Context = new TContext();
        }

        /// <summary>
        /// Gets the context.
        /// </summary>
        protected TContext Context { get; }

        /// <summary>
        /// Asynchronously saves all changes.
        /// </summary>
        /// <returns>The number of objects written.</returns>
        public async Task<int> SaveChangesAsync()
        {
            return await this.Context.SaveChangesAsync();
        }

        /// <summary>
        /// Asynchronously saves all changes.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>
        /// The number of objects written.
        /// </returns>
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await this.Context.SaveChangesAsync(cancellationToken);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Context.Dispose();
        }
    }
}
