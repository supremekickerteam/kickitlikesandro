﻿//-----------------------------------------------------------------------
// <copyright file="AsyncEventHandlerExtensions.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.Utility
{
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Handler for async event subscriptions.
    /// </summary>
    /// <returns>The async tasks</returns>
    public delegate Task AsyncEventHandler();

    /// <summary>
    /// Extensions methods for the <see cref="AsyncEventHandler"/> delegate.
    /// </summary>
    public static class AsyncEventHandlerExtensions
    {
        /// <summary>
        /// Notifies the specified handler asynchronously.
        /// </summary>
        /// <param name="handler">The handler.</param>
        /// <returns>The async task.</returns>
        public static async Task NotifyAsync(this AsyncEventHandler handler)
        {
            if (handler is null)
            {
                return;
            }

            await Task.WhenAll(handler.GetInvocationList().Cast<AsyncEventHandler>().Select(h => h()));
        }
    }
}
