﻿//-----------------------------------------------------------------------
// <copyright file="DateTimeExtensions.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.Common.Utility
{
    using System;

    /// <summary>
    /// Extensions for the <see cref="DateTime"/> class
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// Forces the specified date-time to UTC.
        /// </summary>
        /// <param name="dateTime">The date time.</param>
        /// <returns>The date time forced to UTC.</returns>
        public static DateTime ForceToUtc(this DateTime dateTime)
        {
            switch (dateTime.Kind)
            {
                case DateTimeKind.Utc:
                    return dateTime;

                case DateTimeKind.Unspecified:
                    return DateTime.SpecifyKind(dateTime, DateTimeKind.Utc);

                default:
                    return dateTime.ToUniversalTime();
            }
        }
    }
}
