﻿//-----------------------------------------------------------------------
// <copyright file="TableSoccerRepository.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.AzureFunctions.Common.Persistence
{
    using System.Data.Entity;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.DomainModel;

    /// <summary>
    /// The database-based repository for table soccer entities.
    /// </summary>
    internal sealed class TableSoccerRepository : RepositoryBaseMixin<DatabaseContext>,
        IRepository<SpontaneousMatch>,
        IRepository<SpontaneousMatchHistory>
    {
        /// <summary>
        /// Gets the entities.
        /// </summary>
        IDbSet<SpontaneousMatch> IRepository<SpontaneousMatch>.Entities => this.Context.SpontaneousMatches;


        /// <summary>
        /// Gets the entities.
        /// </summary>
        IDbSet<SpontaneousMatchHistory> IRepository<SpontaneousMatchHistory>.Entities => this.Context.SpontaneousMatchHistory;
    }
}
