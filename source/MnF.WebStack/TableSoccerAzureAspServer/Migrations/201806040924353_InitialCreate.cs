namespace TableSoccerAzureAspServer.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// Initial Creation of tables
    /// </summary>
    /// <seealso cref="System.Data.Entity.Migrations.DbMigration" />
    /// <seealso cref="System.Data.Entity.Migrations.Infrastructure.IMigrationMetadata" />
    public partial class InitialCreate : DbMigration
    {
        /// <summary>
        /// Operations to be performed during the upgrade process.
        /// </summary>
        public override void Up()
        {
            this.CreateTable(
                "dbo.SpontaneousMatches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeStampCreated = c.DateTime(nullable: false),
                        TimeStampCompleted = c.DateTime(),
                        ScoreBlue = c.Int(nullable: false),
                        ScoreRed = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.Matches",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimeStamp = c.DateTime(nullable: false),
                        RedPlayerOneId = c.Int(nullable: false),
                        RedPlayerTwoId = c.Int(),
                        BluePlayerOneId = c.Int(nullable: false),
                        BluePlayerTwoId = c.Int(),
                        IsCompleted = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.BluePlayerOneId)
                .ForeignKey("dbo.Players", t => t.BluePlayerTwoId)
                .ForeignKey("dbo.Players", t => t.RedPlayerOneId)
                .ForeignKey("dbo.Players", t => t.RedPlayerTwoId)
                .Index(t => t.RedPlayerOneId)
                .Index(t => t.RedPlayerTwoId)
                .Index(t => t.BluePlayerOneId)
                .Index(t => t.BluePlayerTwoId);

            this.CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        LastName = c.String(nullable: false, maxLength: 50),
                        EloCount = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            this.CreateTable(
                "dbo.Sets",
                c => new
                    {
                        MatchId = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        RedScore = c.Int(nullable: false),
                        BlueScore = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => new { t.MatchId, t.Number })
                .ForeignKey("dbo.Matches", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.MatchId);

            this.CreateTable(
                "dbo.MatchChanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChangeTimeStamp = c.DateTime(nullable: false),
                        MatchId = c.Int(nullable: false),
                        OldTimeStamp = c.DateTime(nullable: false),
                        OldRedPlayerOneId = c.Int(nullable: false),
                        OldRedPlayerTwoId = c.Int(),
                        OldBluePlayerOneId = c.Int(nullable: false),
                        OldBluePlayerTwoId = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.OldBluePlayerOneId)
                .ForeignKey("dbo.Players", t => t.OldBluePlayerTwoId)
                .ForeignKey("dbo.Matches", t => t.MatchId)
                .ForeignKey("dbo.Players", t => t.OldRedPlayerOneId)
                .ForeignKey("dbo.Players", t => t.OldRedPlayerTwoId)
                .Index(t => t.MatchId)
                .Index(t => t.OldRedPlayerOneId)
                .Index(t => t.OldRedPlayerTwoId)
                .Index(t => t.OldBluePlayerOneId)
                .Index(t => t.OldBluePlayerTwoId);

            this.CreateTable(
                "dbo.SetChanges",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChangeTimeStamp = c.DateTime(nullable: false),
                        MatchId = c.Int(nullable: false),
                        Number = c.Int(nullable: false),
                        OldRedScore = c.Int(nullable: false),
                        OldBlueScore = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Matches", t => t.MatchId)
                .Index(t => t.MatchId);
            
        }

        /// <summary>
        /// Operations to be performed during the downgrade process.
        /// </summary>
        public override void Down()
        {
            this.DropForeignKey("dbo.SetChanges", "MatchId", "dbo.Matches");
            this.DropForeignKey("dbo.MatchChanges", "OldRedPlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.MatchChanges", "OldRedPlayerOneId", "dbo.Players");
            this.DropForeignKey("dbo.MatchChanges", "MatchId", "dbo.Matches");
            this.DropForeignKey("dbo.MatchChanges", "OldBluePlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.MatchChanges", "OldBluePlayerOneId", "dbo.Players");
            this.DropForeignKey("dbo.Sets", "MatchId", "dbo.Matches");
            this.DropForeignKey("dbo.Matches", "RedPlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.Matches", "RedPlayerOneId", "dbo.Players");
            this.DropForeignKey("dbo.Matches", "BluePlayerTwoId", "dbo.Players");
            this.DropForeignKey("dbo.Matches", "BluePlayerOneId", "dbo.Players");
            this.DropIndex("dbo.SetChanges", new[] { "MatchId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldBluePlayerTwoId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldBluePlayerOneId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldRedPlayerTwoId" });
            this.DropIndex("dbo.MatchChanges", new[] { "OldRedPlayerOneId" });
            this.DropIndex("dbo.MatchChanges", new[] { "MatchId" });
            this.DropIndex("dbo.Sets", new[] { "MatchId" });
            this.DropIndex("dbo.Matches", new[] { "BluePlayerTwoId" });
            this.DropIndex("dbo.Matches", new[] { "BluePlayerOneId" });
            this.DropIndex("dbo.Matches", new[] { "RedPlayerTwoId" });
            this.DropIndex("dbo.Matches", new[] { "RedPlayerOneId" });
            this.DropTable("dbo.SetChanges");
            this.DropTable("dbo.MatchChanges");
            this.DropTable("dbo.Sets");
            this.DropTable("dbo.Players");
            this.DropTable("dbo.Matches");
            this.DropTable("dbo.SpontaneousMatches");
        }
    }
}
