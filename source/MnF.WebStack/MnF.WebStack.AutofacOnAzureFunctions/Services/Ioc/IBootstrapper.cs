﻿namespace MnF.WebStack.AutofacOnAzureFunctions.Services.Ioc
{
    using Autofac;

    public interface IBootstrapper
    {
        Module[] CreateModules();
    }
}