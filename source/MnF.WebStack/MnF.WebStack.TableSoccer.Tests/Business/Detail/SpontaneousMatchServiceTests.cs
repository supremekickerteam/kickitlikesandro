﻿//-----------------------------------------------------------------------
// <copyright file="SpontaneousMatchServiceTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using Autofac.Extras.Moq;

    using AutoMapper;

    using FluentAssertions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.Utility;

    using Moq;

    using NUnit.Framework;

    using SpontaneousMatch = MnF.WebStack.TableSoccer.DomainModel.SpontaneousMatch;

    /// <summary>
    /// Unit tests for the <see cref="MatchService"/> class.
    /// </summary>
    [TestFixture]
    internal sealed class SpontaneousMatchServiceTests
    {
        private static readonly DateTime Now = DateTime.Now.ForceToUtc();

        private static readonly IMapper Mapper =
            new MapperConfiguration(cfg => cfg.AddProfiles(typeof(SpontaneousMatchService))).CreateMapper();

        private AutoMock mock;

        private List<SpontaneousMatch> testMatches;

        private Mock<DbSet<SpontaneousMatch>> mockMatchDbSet;

        private static IEnumerable<SpontaneousMatch> TestMatches
        {
            get
            {
                yield return new SpontaneousMatch
                                 {
                                     Id = 10,
                                     TimeStampCreated = Now.Subtract(TimeSpan.FromMinutes(1)),
                                     ScoreBlue = 1,
                                     ScoreRed = 7,
                                 };

                yield return new SpontaneousMatch
                                 {
                                     Id = 11,
                                     TimeStampCreated = Now,
                                     ScoreBlue = 4,
                                     ScoreRed = 7,
                                 };

                yield return new SpontaneousMatch
                                 {
                                     Id = 20,
                                     TimeStampCreated = Now.Subtract(TimeSpan.FromHours(2)),
                                     ScoreBlue = 7,
                                     ScoreRed = 4,
                                     TimeStampCompleted = Now
                                 };

                yield return new SpontaneousMatch
                                 {
                                     Id = 30,
                                     TimeStampCreated = Now.Subtract(TimeSpan.FromHours(3)),
                                     ScoreBlue = 7,
                                     ScoreRed = 5,
                                     TimeStampCompleted = Now.Subtract(TimeSpan.FromMinutes(5))
                                 };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();
            this.testMatches = TestMatches.ToList();

            // provide an AutoMapper configuration, loaded with the profiles from the assembly-under-test.
            this.mock.Provide<IConfigurationProvider>(
                new MapperConfiguration(cfg => cfg.AddProfiles(typeof(SpontaneousMatchService))));

            // provide a match repository containing the test matches
            this.mockMatchDbSet = new Mock<DbSet<SpontaneousMatch>>().SetupData(this.testMatches);
            this.mock.Mock<IRepository<SpontaneousMatch>>().SetupGet(r => r.Entities)
                .Returns(this.mockMatchDbSet.Object);
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetCurrentFromEmptyDatabase()
        {
            var localMockDbSet = new Mock<DbSet<SpontaneousMatch>>().SetupData(new List<SpontaneousMatch>());
            this.mock.Mock<IRepository<SpontaneousMatch>>().SetupGet(r => r.Entities).Returns(localMockDbSet.Object);

            var sut = this.mock.Create<SpontaneousMatchService>();
            (await sut.GetCurrentMatch()).Should().BeNull();
        }

        [Test]
        public async Task GetCurrentFromDatabaseWithOnlyCompleted()
        {
            foreach (var match in this.testMatches)
            {
                match.TimeStampCompleted = Now;
            }

            var sut = this.mock.Create<SpontaneousMatchService>();
            (await sut.GetCurrentMatch()).Should().BeNull();
        }

        [Test]
        public async Task GetCurrentGetsCurrent()
        {
            var sut = this.mock.Create<SpontaneousMatchService>();
            var result = await sut.GetCurrentMatch();

            result.Should().NotBeNull();
            var realCurrentMatch = Mapper.Map<SpontaneousMatch, ResourceModel.SpontaneousMatch>(this.testMatches[1]);

            result.Should().BeEquivalentTo(realCurrentMatch);
        }
    }
}
