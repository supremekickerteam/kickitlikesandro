﻿//-----------------------------------------------------------------------
// <copyright file="SetControllerTests.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.WebApi
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http.Results;

    using Autofac.Extras.Moq;

    using FluentAssertions;

    using MnF.WebStack.TableSoccer.Business;
    using MnF.WebStack.TableSoccer.ResourceModel;
    using MnF.WebStack.WebApi.WebApi;

    using Moq;

    using NUnit.Framework;

    /// <summary>
    /// Unit tests for the <see cref="SetController"/> class.
    /// </summary>
    internal sealed class SetControllerTests
    {
        private AutoMock mock;

        private static IEnumerable<Set> TestSets
        {
            get
            {
                var id = 1;
                yield return new Set
                {
                    MatchId = id++,
                    Number = 1,
                    RedScore = 1,
                    BlueScore = 2,
                };
                yield return new Set
                {
                    MatchId = id,
                    Number = 1,
                    RedScore = 5,
                    BlueScore = 2,
                };
                yield return new Set
                {
                    MatchId = id++,
                    Number = 2,
                    RedScore = 1,
                    BlueScore = 7,
                };
                yield return new Set
                {
                    MatchId = id,
                    Number = 1,
                    RedScore = 1,
                    BlueScore = 6,
                };
            }
        }

        [SetUp]
        public void SetUp()
        {
            this.mock = AutoMock.GetLoose();

            // provide a player service serving the test players
            this.mock.Mock<ISetService>().Setup(s => s.GetAll())
                .Returns(Task.FromResult(TestSets));

            this.mock.Mock<ISetService>().Setup(s => s.GetByMatchId(It.IsAny<int>()))
                .Returns((int i) => Task.FromResult(TestSets.Where(p => p.MatchId == i)));

            this.mock.Mock<ISetService>().Setup(s => s.GetByPk(It.IsAny<int>(), It.IsAny<int>()))
                .Returns((int matchId, int number) => Task.FromResult(TestSets.FirstOrDefault(s => s.MatchId == matchId && s.Number == number)));
        }

        [TearDown]
        public void TearDown()
        {
            this.mock.Dispose();
        }

        [Test]
        public async Task GetAllSuccess()
        {
            var sut = this.mock.Create<SetController>();
            var result = await sut.GetAll();
            result.Should().BeEquivalentTo(TestSets);
        }

        [Test]
        public async Task GetByPkSuccess()
        {
            var sut = this.mock.Create<SetController>();
            var result = await sut.GetByPk(2, 2) as OkNegotiatedContentResult<Set>;
            result.Should().NotBeNull();

            // ReSharper disable once PossibleNullReferenceException
            result.Content.Should().BeEquivalentTo(TestSets.ToArray()[2]);
        }

        [Test]
        public async Task GetByMatchIdSuccess()
        {
            var sut = this.mock.Create<SetController>();
            var resultRaw = await sut.GetByMatchId(2);
            var result = resultRaw as OkNegotiatedContentResult<IEnumerable<Set>>;
            result.Should().NotBeNull();

            // ReSharper disable once PossibleNullReferenceException
            var resultContent = result.Content.ToList();
            resultContent.Should().NotBeEmpty();
            var expectedSet = resultContent[0];

            TestSets.Should().Contain(s => s.Number == expectedSet.Number && s.MatchId == 2);
        }

        [Test]
        public async Task GetByMatchIdNotFound()
        {
            var sut = this.mock.Create<SetController>();
            (await sut.GetByMatchId(4711))
                .Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task GetByPkNotFound()
        {
            var sut = this.mock.Create<SetController>();
            (await sut.GetByPk(4711, 1)).Should().BeOfType<NotFoundResult>();
            (await sut.GetByPk(1, 666)).Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task PostAddsToService()
        {
            var sut = this.mock.Create<SetController>();

            var set = new Set
            {
                MatchId = 3,
                Number = 2,
                RedScore = 6,
                BlueScore = 7,
            };

            await sut.PostNew(set);

            this.mock.Mock<ISetService>().Verify(service => service.Add(It.Is((Set s) => s.Equals(set))), Times.Once);
        }

        [Test]
        public async Task PostSuccessReturnsCreated()
        {
            var sut = this.mock.Create<SetController>();
            var set = new Set
            {
                MatchId = 1,
                Number = 1,
                RedScore = 6,
                BlueScore = 7,
            };

            (await sut.PostNew(set)).Should().BeOfType<CreatedAtRouteNegotiatedContentResult<Set>>();
        }

        [Test]
        public async Task PutUpdatesToService()
        {
            var sut = this.mock.Create<SetController>();

            var set = new Set
            {
                MatchId = 1,
                Number = 1,
                RedScore = 6,
                BlueScore = 7,
            };

            await sut.PutUpdate(set);

            this.mock.Mock<ISetService>().Verify(service => service.Update(It.Is((Set s) => s.Equals(set))), Times.Once);
        }

        [Test]
        public async Task PutSuccessReturnsOk()
        {
            var sut = this.mock.Create<SetController>();
            var set = new Set
            {
                MatchId = 1,
                Number = 1,
                RedScore = 6,
                BlueScore = 7,
            };

            var result = await sut.PutUpdate(set);
            result.Should().BeOfType<OkNegotiatedContentResult<Set>>();
        }

        [Test]
        public async Task PutUnknownReturnsNotFound()
        {
            var sut = this.mock.Create<SetController>();

            this.mock.Mock<ISetService>().Setup(s => s.Update(It.IsAny<Set>())).Throws<KeyNotFoundException>();

            var set = new Set
            {
                MatchId = 666,
                Number = 666,
                RedScore = 6,
                BlueScore = 7,
            };

            var result = await sut.PutUpdate(set);
            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task DeleteCallsService()
        {
            const int Matchid = 42;
            const int Number = 1;
            var sut = this.mock.Create<SetController>();
            await sut.Delete(Matchid, Number);

            this.mock.Mock<ISetService>()
                .Verify(s => s.Delete(It.Is((int matchId) => matchId == Matchid), It.Is((int number) => number == Number)), Times.Once);
        }
    }
}
