﻿//-----------------------------------------------------------------------
// <copyright file="Match.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.ResourceModel
{
    using System;

    /// <summary>
    /// Resource representing a match.
    /// </summary>
    public sealed class Match
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the time stamp.
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the red player one.
        /// </summary>
        public int RedPlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the red player two.
        /// </summary>
        public int RedPlayerTwoId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the blue player one.
        /// </summary>
        public int BluePlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the blue player two.
        /// </summary>
        public int BluePlayerTwoId { get; set; }
    }
}
