﻿//-----------------------------------------------------------------------
// <copyright file="MatchRead.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.ResourceModel
{
    using System.Collections.Generic;

    /// <summary>
    /// Resource representing a match.
    /// </summary>
    public sealed class MatchRead
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        public System.DateTime TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        public Player BluePlayerOne { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        public Player BluePlayerTwo { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        public Player RedPlayerOne { get; set; }

        /// <summary>
        /// Gets or sets the first player of blue team.
        /// </summary>
        public Player RedPlayerTwo { get; set; }

        /// <summary>
        /// Gets or sets the sets.
        /// </summary>
        public ICollection<Set> Sets { get; set; } = new List<Set>();
    }
}
