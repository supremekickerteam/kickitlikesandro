﻿//-----------------------------------------------------------------------
// <copyright file="ISetService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="Set" /> resource.
    /// </summary>
    public interface ISetService
    {
        /// <summary>
        /// Gets all sets.
        /// </summary>
        /// <returns>All sets.</returns>
        Task<IEnumerable<Set>> GetAll();

        /// <summary>
        /// Gets all sets of a match.
        /// </summary>
        /// <param name="matchId">The match identifier.</param>
        /// <returns>
        /// All sets.
        /// </returns>
        Task<IEnumerable<Set>> GetByMatchId(int matchId);

        /// <summary>
        /// Gets the by pk.
        /// </summary>
        /// <param name="matchId">The match identifier.</param>
        /// <param name="setNumber">The set number.</param>
        /// <returns>The corresponding set</returns>
        Task<Set> GetByPk(int matchId, int setNumber);

        /// <summary>
        /// Adds the specified set.
        /// </summary>
        /// <param name="set">The set.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <remarks>
        /// After successfully adding the set, its identifier is set.
        /// </remarks>
        Task Add(Set set);

        /// <summary>
        /// Updates the specified set.
        /// </summary>
        /// <param name="set">The set.</param>
        /// <returns>The async task.</returns>
        /// <exception cref="KeyNotFoundException">Thrown when the specified set is unknown.</exception>
        Task Update(Set set);

        /// <summary>
        /// Deletes the specified match identifier.
        /// </summary>
        /// <param name="matchId">The match identifier.</param>
        /// <param name="number">The number.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        Task Delete(int matchId, int number);
    }
}
