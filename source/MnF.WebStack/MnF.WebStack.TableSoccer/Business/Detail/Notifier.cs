﻿//-----------------------------------------------------------------------
// <copyright file="Notifier.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using MnF.WebStack.Common.Utility;

    /// <summary>
    /// Notifies about changes to the model.
    /// </summary>
    /// <seealso cref="MnF.WebStack.TableSoccer.Business.INotifier" />
    public class Notifier : INotifier
    {
        /// <summary>
        /// Occurs when [model changed].
        /// </summary>
        public event AsyncEventHandler ModelChanged;

        /// <summary>
        /// Notifies the change.
        /// </summary>
        public void NotifyChange()
        {
            this.ModelChanged?.Invoke();
        }
    }
}
