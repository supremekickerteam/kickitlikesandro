﻿//-----------------------------------------------------------------------
// <copyright file="SetService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business.Detail
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="ResourceModel.Set"/> resource.
    /// </summary>
    internal sealed class SetService : ISetService
    {
        private readonly IRepository<DomainModel.Set> setRepository;
        private readonly IRepository<DomainModel.SetChange> setChangeRepository;
        private readonly IConfigurationProvider mapperConfig;

        /// <summary>
        /// Initializes a new instance of the <see cref="SetService" /> class.
        /// </summary>
        /// <param name="setRepository">The set repository.</param>
        /// <param name="setChangeRepository">The set change repository.</param>
        /// <param name="mapperConfig">The mapper configuration.</param>
        public SetService(IRepository<DomainModel.Set> setRepository, IRepository<DomainModel.SetChange> setChangeRepository, IConfigurationProvider mapperConfig)
        {
            this.setRepository = setRepository;
            this.setChangeRepository = setChangeRepository;
            this.mapperConfig = mapperConfig;
        }

        /// <summary>
        /// Gets all sets.
        /// </summary>
        /// <returns>
        /// All sets.
        /// </returns>
        public async Task<IEnumerable<Set>> GetAll()
        {
            return await this.setRepository.Entities
                       .Where(s => !s.IsDeleted)
                       .OrderByDescending(set => (set.Match == null ? new DateTime(0) : set.Match.TimeStamp))
                       .ThenBy(set => set.Number)
                       .ProjectTo<Set>(this.mapperConfig)
                       .ToListAsync();
        }

        /// <summary>
        /// Gets the single set with the specified identifier.
        /// </summary>
        /// <param name="matchId">The set identifier.</param>
        /// <returns>
        /// The set; or <c>null</c> if the specified identifier is unknown.
        /// </returns>
        public async Task<IEnumerable<Set>> GetByMatchId(int matchId)
        {
            return await this.setRepository.Entities
                       .Where(s => !s.IsDeleted)
                       .Where(set => set.MatchId == matchId)
                       .ProjectTo<Set>(this.mapperConfig)
                       .ToListAsync();
        }

        /// <summary>
        /// Gets the single set with the specified identifier.
        /// </summary>
        /// <param name="matchId">The set identifier.</param>
        /// <param name="setNumber">The set number.</param>
        /// <returns>
        /// The set; or <c>null</c> if the specified identifier is unknown.
        /// </returns>
        public async Task<Set> GetByPk(int matchId, int setNumber)
        {
            return await this.setRepository.Entities
                       .Where(s => !s.IsDeleted)
                       .Where(set => set.MatchId == matchId && set.Number == setNumber)
                       .ProjectTo<Set>(this.mapperConfig)
                       .SingleOrDefaultAsync();
        }

        /// <summary>
        /// Adds the specified set.
        /// </summary>
        /// <param name="set">The set.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <remarks>
        /// After successfully adding the set, its identifier is set.
        /// </remarks>
        public async Task Add(Set set)
        {
            var domain = this.mapperConfig.CreateMapper().Map<Set, DomainModel.Set>(set);

            this.setRepository.Entities.Add(domain);
            await this.setRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the specified set.
        /// </summary>
        /// <param name="set">The set.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        /// <exception cref="KeyNotFoundException">Thrown when the specified set is unknown.</exception>
        public async Task Update(Set set)
        {
            var domain = await this.setRepository.Entities.SingleOrDefaultAsync(s => s.MatchId == set.MatchId && s.Number == set.Number);

            if (domain is null)
            {
                throw new KeyNotFoundException($"No set found with PK [{set.MatchId}, {set.Number}]");
            }

            await this.SaveChangeEntry(domain);

            this.mapperConfig.CreateMapper().Map(set, domain);
            await this.setRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes the specified set identifier.
        /// </summary>
        /// <param name="matchId">The set identifier.</param>
        /// <param name="number">The number.</param>
        /// <returns>
        /// The async task.
        /// </returns>
        public async Task Delete(int matchId, int number)
        {
            var domain = await this.setRepository.Entities.SingleOrDefaultAsync(s => s.MatchId == matchId && s.Number == number);
            if (domain is null)
            {
                return;
            }

            if (domain.Match.IsCompleted)
            {
                throw new ActionNotSupportedException("This set belongs to a set which is already completed! Therefore, it cannot be deleted");
            }

            await this.SaveChangeEntry(domain);

            domain.IsDeleted = true;
            await this.setRepository.SaveChangesAsync();
        }

        private async Task SaveChangeEntry(DomainModel.Set set)
        {
            var changeEntry = this.mapperConfig.CreateMapper().Map<DomainModel.Set, DomainModel.SetChange>(set);
            changeEntry.ChangeTimeStamp = DateTime.Now;

            this.setChangeRepository.Entities.Add(changeEntry);
            await this.setChangeRepository.SaveChangesAsync();
        }
    }
}
