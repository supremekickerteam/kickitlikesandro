﻿//-----------------------------------------------------------------------
// <copyright file="IMatchService.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Manages <see cref="MatchWrite"/> and <see cref="MatchRead"/> resources.
    /// </summary>
    public interface IMatchService
    {
        /// <summary>
        /// Gets all matchs.
        /// </summary>
        /// <returns>All matchs.</returns>
        Task<IEnumerable<MatchRead>> GetAll();

        /// <summary>
        /// Gets the single match with the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The match; or <c>null</c> if the specified identifier is unknown.</returns>
        Task<MatchRead> GetById(int id);

        /// <summary>
        /// Adds the specified match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns>The async task.</returns>
        /// <remarks>After successfully adding the match, its identifier is set.</remarks>
        Task Add(MatchWrite match);

        /// <summary>
        /// Updates the specified match.
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns>The async task.</returns>
        /// <exception cref="KeyNotFoundException">Thrown when the specified match is unknown.</exception>
        Task Update(MatchWrite match);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The async task.</returns>
        Task Delete(int id);
    }
}
