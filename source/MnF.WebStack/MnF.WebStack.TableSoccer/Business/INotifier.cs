﻿//-----------------------------------------------------------------------
// <copyright file="INotifier.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Business
{
    using MnF.WebStack.Common.Utility;

    /// <summary>
    /// Notifies events on the table soccer model.
    /// </summary>
    public interface INotifier
    {
        /// <summary>
        /// Occurs when [model changed].
        /// </summary>
        event AsyncEventHandler ModelChanged;
    }
}
