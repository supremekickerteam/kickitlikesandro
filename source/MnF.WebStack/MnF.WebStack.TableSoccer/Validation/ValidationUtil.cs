﻿//-----------------------------------------------------------------------
// <copyright file="ValidationUtil.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System.Linq;

    using FluentValidation;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Utility-class for extension methods for validation
    /// </summary>
    public static class ValidationUtil
    {
        /// <summary>
        /// Ensures that this player exists in the database
        /// </summary>
        /// <param name="ruleBuilder">The rule builder.</param>
        /// <param name="playerRepository">The player repository.</param>
        /// <returns>the new RuleBuilder</returns>
        public static IRuleBuilderOptions<MatchWrite, int?> PlayerMustExist(
            this IRuleBuilder<MatchWrite, int?> ruleBuilder, IRepository<DomainModel.Player> playerRepository)
        {
            return ruleBuilder
                .Must(id => PlayerNullOrExists(playerRepository, id))
                .WithMessage((match, id) => $"Player {id} must exist")
                ;
        }

        /// <summary>
        /// Players the must not be duplicate.
        /// </summary>
        /// <param name="ruleBuilder">The rule builder.</param>
        /// <returns>The rule-builder for the next step</returns>
        public static IRuleBuilderOptions<MatchWrite, MatchWrite> PlayerMustNotBeDuplicate(
            this IRuleBuilder<MatchWrite, MatchWrite> ruleBuilder)
        {
            return ruleBuilder
                .Must(ArePlayersUnique)
                .WithMessage("Cannot set the same player on two positions")
                ;
        }

        /// <summary>
        /// Matches the must not be completed.
        /// </summary>
        /// <typeparam name="TResourceEntity">The type of the resource entity.</typeparam>
        /// <param name="ruleBuilder">The rule builder.</param>
        /// <param name="matchRepository">The match repository.</param>
        /// <returns>The new builder</returns>
        public static IRuleBuilderOptions<TResourceEntity, int?> MatchMustNotBeCompleted<TResourceEntity>(
            this IRuleBuilder<TResourceEntity, int?> ruleBuilder, IRepository<DomainModel.Match> matchRepository)
        {
            return ruleBuilder
                    .Must(matchId => !matchRepository.Entities.Any(match => match.Id == matchId && match.IsCompleted))
                    .WithMessage((entry, id) => $"The match with ID = {id} is already completed and can no longer be changed")
                ;
        }

        /// <summary>
        /// Checks if the Player-IDs are unique
        /// </summary>
        /// <param name="match">The match.</param>
        /// <returns>Whether the players are unique</returns>
        public static bool ArePlayersUnique(this MatchWrite match)
        {
            int?[] ids = { match.BluePlayerOneId, match.BluePlayerTwoId, match.RedPlayerOneId, match.RedPlayerTwoId };
            var noDuplicates = ids
                .Where(item => item != null)
                .GroupBy(i => i)
                .Count(g => g.Count() > 1);
            return noDuplicates == 0;
        }

        private static bool PlayerNullOrExists(IRepository<DomainModel.Player> repository, int? id)
        {
            return id == null || repository.Entities.Any(entry => entry.Id == id);
        }
    }
}
