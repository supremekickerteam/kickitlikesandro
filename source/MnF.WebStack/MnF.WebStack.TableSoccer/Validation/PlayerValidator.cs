﻿//-----------------------------------------------------------------------
// <copyright file="PlayerValidator.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Validation
{
    using System.Data.Entity;
    using System.Linq;

    using FluentValidation;

    using MnF.WebStack.Common.DataAccess;
    using MnF.WebStack.Common.WebApi.Validation;
    using MnF.WebStack.TableSoccer.ResourceModel;

    /// <summary>
    /// Validator to validate incoming Player-objects
    /// </summary>
    public class PlayerValidator : AbstractTsValidator<Player, DomainModel.Player>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerValidator" /> class.
        /// </summary>
        /// <param name="playerRepository">The player repository.</param>
        public PlayerValidator(IRepository<DomainModel.Player> playerRepository)
            : base(playerRepository)
        {
            this.RuleFor(m => m.FirstName).NotEmpty().WithMessage("Please enter your first name");
            this.RuleFor(m => m.LastName).NotEmpty().WithMessage("Please enter your last name");
        }

        /// <summary>
        /// See "returns" part
        /// </summary>
        /// <param name="resource">The resource.</param>
        /// <param name="entities">The entities.</param>
        /// <returns>
        /// true if the entry exists in the database
        /// </returns>
        protected override bool DoesEntryExist(Player resource, IDbSet<DomainModel.Player> entities)
        {
            return entities.Any(domain => resource.Id == domain.Id);
        }
    }
}
