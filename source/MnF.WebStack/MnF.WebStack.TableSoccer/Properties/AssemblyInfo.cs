﻿//-----------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MnF.WebStack.TableSoccer")]
[assembly: AssemblyDescription("The table soccer component.")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// Enable testing of the internal stuff
[assembly:InternalsVisibleTo("Mnf.WebStack.TableSoccer.Tests")]
