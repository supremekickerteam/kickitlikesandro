﻿//-----------------------------------------------------------------------
// <copyright file="SpontaneousMatchHistory.cs" company="M+F Engineering AG">
// Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System;
    using MnF.WebStack.Common.Utility;

    /// <summary>
    /// Represents a table soccer match.
    /// </summary>
    public class SpontaneousMatchHistory
    {
        private DateTime timeStamp;

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the time stamp in UTC.
        /// </summary>
        public DateTime TimeStamp
        {
            get => this.timeStamp;
            set => this.timeStamp = value.ForceToUtc();
        }

        /// <summary>
        /// Gets or sets the score blue.
        /// </summary>
        public int ScoreBlueDiff { get; set; }

        /// <summary>
        /// Gets or sets the score red.
        /// </summary>
        public int ScoreRedDiff { get; set; }
    }
}
