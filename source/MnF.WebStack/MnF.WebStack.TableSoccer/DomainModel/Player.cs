﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System.ComponentModel;

    /// <summary>
    /// A table soccer player.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the elo count of the player.
        /// </summary>
        public int EloCount { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this player is marked as deleted.
        /// </summary>
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
    }
}
