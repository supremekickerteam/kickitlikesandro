﻿//-----------------------------------------------------------------------
// <copyright file="Set.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System.ComponentModel;

    /// <summary>
    /// Represents a set of a <see cref="Match"/>.
    /// </summary>
    public class Set
    {
        /// <summary>
        /// Gets or sets the match identifier.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the number of this set in the match.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the score of the red team.
        /// </summary>
        public int RedScore { get; set; }

        /// <summary>
        /// Gets or sets the score of the blue team.
        /// </summary>
        public int BlueScore { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the match.
        /// </summary>
        public virtual Match Match { get; set; }
    }
}
