﻿//-----------------------------------------------------------------------
// <copyright file="MatchChange.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.DomainModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using MnF.WebStack.Common.Utility;

    /// <summary>
    /// Represents a table soccer match.
    /// </summary>
    public class MatchChange
    {
        private DateTime timeStamp;
        private DateTime changeTimeStamp;

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the change time stamp.
        /// </summary>
        public DateTime ChangeTimeStamp
        {
            get => this.changeTimeStamp;
            set => this.changeTimeStamp = value.ForceToUtc();
        }

        /// <summary>
        /// Gets or sets the match identifier.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the match.
        /// </summary>
        public virtual Match Match { get; set; }

        /// <summary>
        /// Gets or sets the time stamp in UTC.
        /// </summary>
        [Column("OldTimeStamp")]
        public DateTime TimeStamp
        {
            get => this.timeStamp;
            set => this.timeStamp = value.ForceToUtc();
        }

        /// <summary>
        /// Gets or sets the identifier of the red player one.
        /// </summary>
        [Column("OldRedPlayerOneId")]
        public int RedPlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the red player two.
        /// </summary>
        [Column("OldRedPlayerTwoId")]
        public int? RedPlayerTwoId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the blue player one.
        /// </summary>
        [Column("OldBluePlayerOneId")]
        public int BluePlayerOneId { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the blue player two.
        /// </summary>
        [Column("OldBluePlayerTwoId")]
        public int? BluePlayerTwoId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the red player one.
        /// </summary>
        public virtual Player RedPlayerOne { get; set; }

        /// <summary>
        /// Gets or sets the red player two.
        /// </summary>
        public virtual Player RedPlayerTwo { get; set; }

        /// <summary>
        /// Gets or sets the blue player one.
        /// </summary>
        public virtual Player BluePlayerOne { get; set; }

        /// <summary>
        /// Gets or sets the blue player two.
        /// </summary>
        public virtual Player BluePlayerTwo { get; set; }
    }
}
