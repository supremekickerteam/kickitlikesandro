﻿//-----------------------------------------------------------------------
// <copyright file="MappingProfile.cs" company="M+F Engineering AG">
//     Copyright (c) M+F Engineering AG. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace MnF.WebStack.TableSoccer.Component
{
    using AutoMapper;

    /// <summary>
    /// The <see cref="AutoMapper"/> profile if the <see cref="TableSoccer"/> component.
    /// </summary>
    public sealed class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MappingProfile"/> class.
        /// </summary>
        public MappingProfile()
        {
            this.MapPlayer();
            this.MapMatch();
            this.MapSet();
        }

        private void MapPlayer()
        {
            this.CreateMap<ResourceModel.Player, DomainModel.Player>().ReverseMap();
        }

        private void MapMatch()
        {
            this.CreateMap<ResourceModel.MatchWrite, DomainModel.Match>().ReverseMap();
            this.CreateMap<ResourceModel.MatchRead, DomainModel.Match>().ReverseMap();

            this.CreateMap<DomainModel.Match, DomainModel.MatchChange>()
                .ForMember(dest => dest.MatchId, option => option.MapFrom(src => src.Id))
                .IgnoreMember(dest => dest.Id)
                .IgnoreMember(dest => dest.ChangeTimeStamp)
                ;

            this.CreateMap<ResourceModel.SpontaneousMatch, DomainModel.SpontaneousMatch>().ReverseMap();
        }

        private void MapSet()
        {
            this.CreateMap<ResourceModel.Set, DomainModel.Set>().ReverseMap();

            this.CreateMap<DomainModel.Set, DomainModel.SetChange>()
            .IgnoreMember(dest => dest.Id)
            .IgnoreMember(dest => dest.ChangeTimeStamp)
            ;
        }
    }
}
